#include <allegro.h>
#include <stdlib.h>

using namespace std;

//variables
bool salida,teo;
int indice;
int puntos;
int aumento_d_puntaje;
int notaAleatoria;
BITMAP *buffer;//buffer
BITMAP *cursor;//contiene imagen del cursor

BITMAP *inicio;//------------|
BITMAP *inicio2;//           | contienen imagenes del menu
BITMAP *inicio3;//           |
BITMAP *inicio4;//-----------|

BITMAP *teoria1;
BITMAP *teoria2;
BITMAP *teoria3;
BITMAP *teoria4;
BITMAP *teoria5;
BITMAP *teoria6;
BITMAP *teoria7;

BITMAP *piano;
BITMAP *piano2;
BITMAP *piano2_1;
BITMAP *piano3;
BITMAP *piano3_1;
BITMAP *piano4;
BITMAP *piano4_1;
BITMAP *piano5;
BITMAP *piano5_1;
BITMAP *piano6;
BITMAP *piano6_1;
BITMAP *piano7;
BITMAP *piano7_1;
BITMAP *piano8;
BITMAP *piano8_1;
BITMAP *piano9;
BITMAP *piano10;
BITMAP *piano11;
BITMAP *piano12;
BITMAP *piano13;
BITMAP *piano14;
BITMAP *piano15;
BITMAP *piano16;
BITMAP *puntaje_img;
BITMAP *pianoN2;
BITMAP *integrantes;


SAMPLE *sonido1;
SAMPLE *sonido2;
SAMPLE *sonido3;
SAMPLE *sonido4;
SAMPLE *sonido5;
SAMPLE *sonido6;
SAMPLE *sonido7;

//funciones prototipo
void iniciarAllegro();
void iniciacionVariables();
void demoTeoria(BITMAP t[]);
void menu();
void demoNotas();
void destruir_liberarmemoria();
//***************************************main***********************************
int main()
{
    iniciarAllegro();
    iniciacionVariables();
    // incializa el audio en allegro
    if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL) != 0)
    {
        allegro_message("Error: inicializando sistema de sonido\n%s\n", allegro_error);
        return 1;
    }

    while(!salida) //inicia el while de todo el programa
    {
        menu();
        indice=0;//************|
        notaAleatoria=0;//*****|
        aumento_d_puntaje=0;//*|
        teo=true;//************||<====se inicializan cuando hay una nueva iteracion para volver a ejecutar nuevamente todo

    }


    readkey();
    destruir_liberarmemoria();
    allegro_exit();
    return 0;
}

END_OF_MAIN();

void destruir_liberarmemoria()
{
    destroy_bitmap(buffer);
    destroy_bitmap(cursor);
    destroy_bitmap(inicio);
    destroy_bitmap(inicio2);
    destroy_bitmap(inicio3);
    destroy_bitmap(inicio4);
    destroy_bitmap(piano);
    destroy_bitmap(piano2);
    destroy_bitmap(piano2_1);
    destroy_bitmap(piano3);
    destroy_bitmap(piano3_1);
    destroy_bitmap(piano4);
    destroy_bitmap(piano4_1);
    destroy_bitmap(piano5);
    destroy_bitmap(piano5_1);
    destroy_bitmap(piano6);
    destroy_bitmap(piano6_1);
    destroy_bitmap(piano7);
    destroy_bitmap(piano7_1);
    destroy_bitmap(piano8);
    destroy_bitmap(piano8_1);
    destroy_bitmap(teoria1);
    destroy_bitmap(teoria2);
    destroy_bitmap(teoria3);
    destroy_bitmap(teoria4);
    destroy_bitmap(teoria5);
    destroy_bitmap(teoria6);
    destroy_bitmap(teoria7);
    destroy_bitmap(piano9);
    destroy_bitmap(piano10);
    destroy_bitmap(piano11);
    destroy_bitmap(piano12);
    destroy_bitmap(piano13);
    destroy_bitmap(piano14);
    destroy_bitmap(piano15);
    destroy_bitmap(piano16);
    destroy_bitmap(puntaje_img);
    destroy_bitmap(pianoN2);
    destroy_bitmap(integrantes);
    //sonidos
    destroy_sample(sonido1);
    destroy_sample(sonido2);
    destroy_sample(sonido3);
    destroy_sample(sonido4);
    destroy_sample(sonido5);
    destroy_sample(sonido6);
    destroy_sample(sonido7);

}

void iniciacionVariables()
{
    text_mode(-1);
    notaAleatoria=0;
    puntos=0;
    aumento_d_puntaje=0;
    indice=0;
    salida=false;
    teo=true;
    cursor=load_bitmap("image/cursor2.bmp",NULL);
    inicio=load_bitmap("image/m1.bmp",NULL);
    inicio2=load_bitmap("image/m2.bmp",NULL);
    inicio3=load_bitmap("image/m3.bmp",NULL);
    inicio4=load_bitmap("image/m4.bmp",NULL);

    piano=load_bitmap("image/piano.bmp",NULL);
    pianoN2=load_bitmap("image/pianoN2.bmp",NULL);
    puntaje_img=load_bitmap("image/puntaje.bmp",NULL);
    integrantes=load_bitmap("image/integrantes.bmp",NULL);
    //NOTA DO
    piano2=load_bitmap("image/piano2.bmp",NULL);
    piano2_1=load_bitmap("image/piano2_1.bmp",NULL);

    //NOTA RE
    piano3=load_bitmap("image/piano3.bmp",NULL);
    piano3_1=load_bitmap("image/piano3_1.bmp",NULL);

    //NOTA MI
    piano4=load_bitmap("image/piano4.bmp",NULL);
    piano4_1=load_bitmap("image/piano4_1.bmp",NULL);

    //NOTA FA
    piano5=load_bitmap("image/piano5.bmp",NULL);
    piano5_1=load_bitmap("image/piano5_1.bmp",NULL);

    //NOTA SOL
    piano6=load_bitmap("image/piano6.bmp",NULL);
    piano6_1=load_bitmap("image/piano6_1.bmp",NULL);

    //NOTA LA
    piano7=load_bitmap("image/piano7.bmp",NULL);
    piano7_1=load_bitmap("image/piano7_1.bmp",NULL);

    //NOTA SI
    piano8=load_bitmap("image/piano8.bmp",NULL);
    piano8_1=load_bitmap("image/piano8_1.bmp",NULL);

    //todas las notas
    piano9=load_bitmap("image/piano9.bmp",NULL);
    piano10=load_bitmap("image/piano10.bmp",NULL);
    piano11=load_bitmap("image/piano11.bmp",NULL);
    piano12=load_bitmap("image/piano12.bmp",NULL);
    piano13=load_bitmap("image/piano13.bmp",NULL);
    piano14=load_bitmap("image/piano14.bmp",NULL);
    piano15=load_bitmap("image/piano15.bmp",NULL);
    piano16=load_bitmap("image/piano16.bmp",NULL);


    teoria1=load_bitmap("image/arreglo/teoria1.BMP",NULL);
    teoria2=load_bitmap("image/arreglo/teoria2.BMP",NULL);
    teoria3=load_bitmap("image/arreglo/teoria3.BMP",NULL);
    teoria4=load_bitmap("image/arreglo/teoria4.BMP",NULL);
    teoria5=load_bitmap("image/arreglo/teoria5.BMP",NULL);
    teoria6=load_bitmap("image/arreglo/teoria6.BMP",NULL);
    teoria7=load_bitmap("image/arreglo/teoria7.BMP",NULL);

    //sonidos
    sonido1 = load_wav("sonido/notado.wav");
    sonido2= load_wav("sonido/notare.wav");
    sonido3 = load_wav("sonido/notami.wav");
    sonido4= load_wav("sonido/notafa.wav");
    sonido5 = load_wav("sonido/notasol.wav");
    sonido6= load_wav("sonido/notala.wav");
    sonido7= load_wav("sonido/notasi.wav");

    buffer = create_bitmap(800,600);//buffer sobre el cual se imprime las imagenes

}
//****************************************************************************
void mostrarTeoria()
{
    BITMAP *teorias[]= {teoria1,teoria2,teoria3,teoria4,teoria5,teoria6,teoria7};


    while(teo)
    {
        if(indice<6)
        {
            demoTeoria(teorias[indice]);
        }
        else
        {
            demoNotas();
            if(key[KEY_ESC])
            {
                teo=false;
            }
        }
    }



}
//*******************************************************************************************
void demoTeoria(BITMAP *t)
{
    blit(t,buffer,0,0,0,0,800,600);
    blit(buffer,screen,0,0,0,0,800,600);
    if(indice==0)
    {
        if(key[KEY_ESC])
        {
            indice=6;
            rest(200);
        }
    }
    if(key[KEY_RIGHT])
    {
        indice++;
        rest(200);
    }


}
//********************************************************************************************
void juego()
{
    while(!key[KEY_ESC])
    {
        srand(time(NULL));
        notaAleatoria=rand()%7;
        aumento_d_puntaje=rand()%20;

        blit(piano,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);
        rest(1000);
        if(notaAleatoria==0)
        {
            play_sample(sonido1,180,127,1000,0);
            while(true)
            {
                if(key[KEY_A])
                {
                    blit(piano2_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido1,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==1)
        {
            play_sample(sonido2,180,127,1000,0);
            while(true)
            {
                if(key[KEY_S])
                {
                    blit(piano3_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido2,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==2)
        {
            play_sample(sonido3,180,127,1000,0);
            while(true)
            {
                if(key[KEY_D])
                {
                    blit(piano4_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido3,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==3)
        {
            play_sample(sonido4,180,127,1000,0);
            while(true)
            {
                if(key[KEY_F])
                {
                    blit(piano5_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido4,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==4)
        {
            play_sample(sonido5,180,127,1000,0);
            while(true)
            {
                if(key[KEY_G])
                {
                    blit(piano6_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido5,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==5)
        {
            play_sample(sonido6,180,127,1000,0);
            while(true)
            {
                if(key[KEY_H])
                {
                    blit(piano7_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido6,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }

        else if(notaAleatoria==6)
        {
            play_sample(sonido7,180,127,1000,0);
            while(true)
            {
                if(key[KEY_J])
                {
                    blit(piano8_1,buffer,0,0,0,0,800,600);
                    blit(buffer,screen,0,0,0,0,800,600);
                    play_sample(sonido7,180,127,1000,0);
                    rest(200);
                    puntos+=aumento_d_puntaje;
                    break;
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        if(puntos>=100)
        {
            play_sample(load_sample("sonido/aplausos.wav"),180,127,1000,0);
            break;
        }
        rest(400);
    }
}

//**********************************************************************************************
void juego2()
{
    while(!key[KEY_ESC])
    {
        srand(time(NULL));
        notaAleatoria=rand()%2;
        aumento_d_puntaje=50;

        blit(pianoN2,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);
        rest(1000);
        if(notaAleatoria==0)
        {
            blit(piano2_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido1,180,127,1000,0);
            rest(200);
            blit(piano7_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido6,180,127,1000,0);
            rest(200);
            blit(pianoN2,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            while(true)
            {
                if(key[KEY_A])
                {

                    if(key[KEY_H])
                    {
                        blit(piano2_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido1,180,127,1000,0);
                        rest(200);
                        blit(piano7_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido6,180,127,1000,0);
                        puntos+=aumento_d_puntaje;
                        break;
                    }
                    if(key[KEY_ESC])
                    {
                        break;
                        break;
                    }
                }
            }
        }


        else if(notaAleatoria==1)
        {
            blit(piano3_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido2,180,127,1000,0);
            rest(200);
            blit(piano4_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido3,180,127,1000,0);
            rest(200);
            blit(pianoN2,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            while(true)
            {
                if(key[KEY_S])
                {

                    if(key[KEY_D])
                    {
                        blit(piano3_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido2,180,127,1000,0);
                        rest(200);
                        blit(piano4_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido3,180,127,1000,0);
                        puntos+=aumento_d_puntaje;
                        break;
                    }
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }
        else if(notaAleatoria==2)
        {
            blit(piano5_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido4,180,127,1000,0);
            rest(200);
            blit(piano2_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido1,180,127,1000,0);
            rest(200);
            blit(pianoN2,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            while(true)
            {
                if(key[KEY_F])
                {

                    if(key[KEY_A])
                    {
                        blit(piano5_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido4,180,127,1000,0);
                        rest(200);
                        blit(piano2_1,buffer,0,0,0,0,800,600);
                        blit(buffer,screen,0,0,0,0,800,600);
                        play_sample(sonido1,180,127,1000,0);
                        puntos+=aumento_d_puntaje;
                        break;
                    }
                }
                if(key[KEY_ESC])
                {
                    break;
                    break;
                }
            }
        }

        if(puntos>=250)
        {
            clear(buffer);
            play_sample(load_sample("sonido/aplausos.wav"),180,127,1000,0);
            while(!key[KEY_ENTER])
            {
                blit(puntaje_img,buffer,0,0,0,0,800,600);
                textprintf(puntaje_img, font, 380,319, palette_color[12],"%d",puntos);
                blit(buffer,screen,0,0,0,0,800,600);

            }
            break;
        }
        rest(200);
    }
}

//********************************************************************************************
void demoNotas()
{


    if(indice==6)
    {
        blit(piano2,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);
        if(key[KEY_A])
        {
            blit(piano2_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido1,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==7)
    {
        blit(piano3,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_S])
        {
            blit(piano3_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido2,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==8)
    {
        blit(piano4,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_D])
        {
            blit(piano4_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido3,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==9)
    {
        blit(piano5,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_F])
        {
            blit(piano5_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido4,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==10)
    {
        blit(piano6,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_G])
        {
            blit(piano6_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido5,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==11)
    {
        blit(piano7,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_H])
        {
            blit(piano7_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido6,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else if(indice==12)
    {
        blit(piano8,buffer,0,0,0,0,800,600);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_J])
        {
            blit(piano8_1,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido7,180,127,1000,0);
            rest(200);
            indice++;
        }
    }
    else
    {

        blit(piano9,buffer,0,0,0,0,800,600);
        draw_sprite(buffer,load_bitmap("image/mensaje.bmp",NULL),52,139);
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_A])
        {
            blit(piano10,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido1,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_S])
        {
            blit(piano11,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido2,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_D])
        {
            blit(piano12,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido3,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_F])
        {
            blit(piano13,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido4,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_G])
        {
            blit(piano14,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido5,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_H])
        {
            blit(piano15,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido6,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_J])
        {
            blit(piano16,buffer,0,0,0,0,800,600);
            blit(buffer,screen,0,0,0,0,800,600);
            play_sample(sonido7,180,127,1000,0);
            rest(200);
        }
        else if(key[KEY_ENTER])
        {
            teo=false;
        }
    }


}
//********************************************************************************************
void menu()
{
    if (mouse_x > 345 && mouse_x < 686 && mouse_y > 176 && mouse_y < 248)
    {
        blit(inicio2,buffer,0,0,0,0,800,600);
        masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,30,31);//pinta a flecha del mouse
        blit(buffer,screen,0,0,0,0,800,600);

        if(mouse_b&1)
        {
            rest(40);
            mostrarTeoria();
            juego();
            juego2();
        }
    }
    else if (mouse_x > 345 && mouse_x < 524 && mouse_y > 284 && mouse_y < 346)
    {
        blit(inicio3,buffer,0,0,0,0,800,600);
        masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,30,31);//pinta a flecha del mouse
        blit(buffer,screen,0,0,0,0,800,600);

        if(key[KEY_END] || mouse_b&1)
        {
            salida=true;

        }
    }
    else if (mouse_x > 36 && mouse_x < 94 && mouse_y > 11 && mouse_y < 207)
    {
        blit(inicio4,buffer,0,0,0,0,800,600);
        masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,30,31);//pinta a flecha del mouse
        blit(buffer,screen,0,0,0,0,800,600);

        if(mouse_b&1)
        {
            while(!key[KEY_ENTER])
            {
                blit(integrantes,buffer,0,0,0,0,800,600);
                blit(buffer,screen,0,0,0,0,800,600);
            }
        }

        if(key[KEY_END])
        {
            salida=true;
        }
    }

    else
    {
        blit(inicio,buffer,0,0,0,0,800,600);
        masked_blit(cursor,buffer,0,0,mouse_x,mouse_y,30,31);//pinta a flecha del mouse
        blit(buffer,screen,0,0,0,0,800,600);
        if(key[KEY_END])
        {
            salida=true;
        }
    }

}
//************************************************************************************

void iniciarAllegro()
{
    allegro_init();
    install_keyboard();
    install_mouse();



    // ajustamos el volumen
    set_volume(230, 200);

    set_color_depth(32);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0,0);

}
